package cz.tjhp;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private CreateConnectThread createConnectThread;
    private ListView bluetoothList;
    private TextView vol, dist;
    private Button connectButton;
    private BluetoothAdapter myBluetooth;
    @SuppressWarnings("unused")
    private BluetoothSocket mmSocket;
    @SuppressWarnings("unused")
    private ConnectedThread connectedThread;
    private String deviceMac;
    private int volValue = 25;
    private int distValue = 40;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button volPlus, volMinus, distPlus, distMinus;
        Button firstButton, secondButton, thirdButton, fourthButton, fifthButton, sixthButton, seventhButton, eightButton, ninethButton, tenthButton;
        Button disconnectButton;

        vol = (TextView) findViewById(R.id.vol);
        dist = (TextView) findViewById(R.id.dist);

        vol.setText(String.valueOf(volValue));
        dist.setText(String.valueOf(distValue));

        connectButton = (Button) findViewById(R.id.connectButton);
        disconnectButton = (Button) findViewById(R.id.disconnectButton);

        firstButton = (Button) findViewById(R.id.firstButton);
        secondButton = (Button) findViewById(R.id.secondButton);
        thirdButton = (Button) findViewById(R.id.thirdButton);
        fourthButton = (Button) findViewById(R.id.fourthButton);
        fifthButton = (Button) findViewById(R.id.fifthButton);
        sixthButton = (Button) findViewById(R.id.sixButton);
        seventhButton = (Button) findViewById(R.id.sevenButton);
        eightButton = (Button) findViewById(R.id.eightButton);
        ninethButton = (Button) findViewById(R.id.nineButton);
        tenthButton = (Button) findViewById(R.id.tenButton);

        distPlus = (Button) findViewById(R.id.plusDist);
        distMinus = (Button) findViewById(R.id.minDist);

        volPlus = (Button) findViewById(R.id.plusVol);
        volMinus = (Button) findViewById(R.id.minVol);


        //Connect and disconnect buttons
        disconnectButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.cancel();
        });
        connectButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.cancel();
        });

        //Distance and volume buttons
        volPlus.setOnClickListener(v -> {
            if(createConnectThread != null){
                createConnectThread.sendMessage("+");
                volValue++;
                vol.setText(String.valueOf(volValue));
            }
        });
        volMinus.setOnClickListener(v -> {
            if(createConnectThread != null) {
                createConnectThread.sendMessage("-");
                volValue--;
                vol.setText(String.valueOf(volValue));
            }
        });
        distPlus.setOnClickListener(v -> {
            if(createConnectThread != null) {
                createConnectThread.sendMessage("p");
                distValue += 10;
                dist.setText(String.valueOf(distValue));
            }
        });
        distMinus.setOnClickListener(v -> {
            if(createConnectThread != null) {
                createConnectThread.sendMessage("m");
                distValue -= 10;
                dist.setText(String.valueOf(distValue));
            }
        });


        firstButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("0");
        });
        secondButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("1");
        });
        thirdButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("2");
        });
        fourthButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("3");
        });
        fifthButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("4");
        });
        sixthButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("5");
        });
        seventhButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("6");
        });
        eightButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("7");
        });
        ninethButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("8");
        });
        tenthButton.setOnClickListener(v -> {
            if(createConnectThread != null)
                createConnectThread.sendMessage("9");
        });

        chceckBTAdapter();
        makeConnection();
    }

    //This method create and testing the Bluetooth Adapter
    private void chceckBTAdapter() {
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if (myBluetooth == null) {
            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
            finish();
        } else {
            if (!myBluetooth.isEnabled()) {
                Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnBTon, 1);
            }
        }
    }

    //Make bluetooth connection with Arduino
    private void makeConnection() {
        bluetoothList = (ListView) findViewById(R.id.bluetoothList);
        connectButton.setOnClickListener(v -> pairedDevicesList());
    }

    //Fetch list of devices to connect
    private void pairedDevicesList() {
        Set<BluetoothDevice> pairedDevices;
        pairedDevices = myBluetooth.getBondedDevices();
        ArrayList<String> list = new ArrayList<>();

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice bt : pairedDevices) {
                list.add(bt.getName() + "\n" + bt.getAddress());
            }
        } else {
            Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found.", Toast.LENGTH_LONG).show();
        }

        AdapterView.OnItemClickListener myListClickListener = (av, v, arg2, arg3) -> {
            String info = ((TextView) v).getText().toString();
            deviceMac = info.substring(info.length() - 17);
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            createConnectThread = new CreateConnectThread(bluetoothAdapter, deviceMac, mmSocket, connectedThread);
            createConnectThread.start();
        };

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        bluetoothList.setAdapter(adapter);
        bluetoothList.setOnItemClickListener(myListClickListener);
    }

    // Terminate Bluetooth Connection and close app
    @Override
    public void onBackPressed() {
        if (createConnectThread != null){
            createConnectThread.cancel();
        }
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    public void onClickedRand(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("x");
    }
    public void onClickedFirst(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("a");
    }
    public void onClickedSecond(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("b");
    }
    public void onClickedThird(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("c");
    }
    public void onClickedFourth(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("d");
    }
    public void onClickedFive(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("e");
    }
    public void onClickedSixth(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("f");
    }
    public void onClickedSeventh(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("g");
    }
    public void onClickedEight(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("h");
    }
    public void onClickedNine(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("i");
    }
    public void onClickedTenth(View view){
        if(createConnectThread != null)
            createConnectThread.sendMessage("j");
    }


}