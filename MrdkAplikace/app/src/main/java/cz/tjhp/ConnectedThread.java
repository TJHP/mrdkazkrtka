package cz.tjhp;

import android.bluetooth.BluetoothSocket;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ConnectedThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final InputStream mmInStream;
    private final OutputStream mmOutStream;
    public String message;

    //Constructor
    public ConnectedThread(BluetoothSocket socket) {

        mmSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            Log.e("StreamError","Error in I/O stream in ConnectedThread class!");
        }

        mmInStream = tmpIn;
        mmOutStream = tmpOut;
    }

    //Run thread
    public void run() {
        byte[] buffer = new byte[1024];
        int bytes = 0;

        while (true) {
            try {
                buffer[bytes] = (byte) mmInStream.read();
                String readMessage;
                if (buffer[bytes] == '\n'){
                    readMessage = new String(buffer,0,bytes);
                    message = readMessage;
                    bytes = 0;
                } else {
                    bytes++;
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    //Send message into Arduino
    public void write(String input) {
        byte[] bytes = input.getBytes();
        try {
            mmOutStream.write(bytes);
        } catch (IOException e) {
            Log.e("Send Error","Unable to send message",e);
        }
    }

    //Close connection
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            Log.e("CancelError","Unable to close the socket",e);
        }
    }

}


