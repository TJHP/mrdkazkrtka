package cz.tjhp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.util.UUID;

class CreateConnectThread extends Thread {

    private BluetoothSocket mmSocket;
    public ConnectedThread connectedThread;

    //Constructor
    public CreateConnectThread(BluetoothAdapter bluetoothAdapter, String address, BluetoothSocket mmSocket, ConnectedThread connectedThread) {
        this.mmSocket = mmSocket;
        this.connectedThread = connectedThread;
        BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(address);
        BluetoothSocket tmp = null;
        UUID uuid = bluetoothDevice.getUuids()[0].getUuid();

        try {
            tmp = bluetoothDevice.createInsecureRfcommSocketToServiceRecord(uuid);

        } catch (IOException e) {
            Log.e("GetDev", "Socket's create() method failed", e);
        }
        this.mmSocket = tmp;
    }

    //Run CreateConnectionThread
    public void run() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothAdapter.cancelDiscovery();
        try {
            mmSocket.connect();
            Log.e("Status", "Device connected");
        } catch (IOException connectException) {
            try {
                mmSocket.close();
                Log.e("Status", "Cannot connect to device");
            } catch (IOException closeException) {
                Log.e("Tag", "Could not close the client socket", closeException);
            }
            return;
        }

        connectedThread = new ConnectedThread(mmSocket);
        connectedThread.run();
    }

    //Close socket and cancel the thread
    public void cancel() {
        try {
            connectedThread.cancel();
            mmSocket.close();
        } catch (IOException e) {
            Log.e("Tag", "Could not close the client socket", e);
        }
    }

    //Send signal to Arduino via ConnectThread class
    public void sendMessage(String s){
        connectedThread.write(s);
    }

}
