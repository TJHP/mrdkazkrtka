/* Libraries */
#include <SoftwareSerial.h>
#include <DFPlayer_Mini_Mp3.h>

/* Define global variables */
SoftwareSerial MP3Serial(10, 11);
SoftwareSerial BTserial(2, 1);
const  byte pTrig = 4;
const  byte pEcho = 5;
byte permanentRecord = 4;
byte countOfRecords = 9;
unsigned int response, distance;
bool playing = false;
char fuckChar;
const byte BTpin = 7;
unsigned short volume = 25;
unsigned short commands = 0;
unsigned short radius = 45;
boolean rand0m = true;

/* Setup block */
void setup () {
  pinMode(pTrig, OUTPUT);
  pinMode(pEcho, INPUT);
  pinMode(BTpin, INPUT);

  Serial.begin (9600);
  MP3Serial.begin (9600);
  BTserial.begin(9600);

  mp3_set_serial (MP3Serial);
  mp3_set_volume (volume);

  randomSeed(analogRead(0));
}

/* Loop block */
void loop () {
  /* Distance sensor functionality */
  digitalWrite(pTrig, LOW);
  delayMicroseconds(2);
  digitalWrite(pTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(pTrig, LOW);
  response = pulseIn(pEcho, HIGH);
  distance = response * 0.034 / 2;
  delay(300);

  /* Obstacle detected! Play the record! */
  if (distance <= radius && !playing) {
    playing = true;
    if(!rand0m)
      mp3_play (permanentRecord);
    else
      mp3_play (random(0, 10));
    delay (6500);
    mp3_stop ();
    playing = false;
  }

  // Keep reading from Arduino Serial Monitor input field and send to HC-05
  if (Serial.available()) {
    fuckChar = Serial.read();
    int num = 100;
    int dflt = permanentRecord;
    switch(fuckChar){
      case '0':
        num = 0;
        break;
      case '1':
        num = 1;
        break;
      case '2':
        num = 2;
        break;
      case '3':
        num = 3;
        break;
      case '4':
        num = 4;
        break;
      case '5':
        num = 5;
        break;
      case '6':
        num = 6;
        break;
      case '7':
        num = 7;
        break;
      case '8':
        num = 8;
        break;
      case '9':
        num = 9;
        break;
      case 'a':
        dflt = 0;
        rand0m = false;
        break;
       case 'b':
        dflt = 1;
        rand0m = false;
        break;
      case 'c':
        dflt = 2;
        rand0m = false;
        break;
      case 'd':
        dflt = 3;
        rand0m = false;
        break;
      case 'e':
        dflt = 4;
        rand0m = false;
        break;
      case 'f':
        dflt = 5;
        rand0m = false;
        break;
      case 'g':
        dflt = 6;
        rand0m = false;
        break;
      case 'h':
        dflt = 7;
        rand0m = false;
        break;
      case 'i':
        dflt = 8;
        rand0m = false;
        break;
      case 'j':
        dflt = 9;
        rand0m = false;
        break;
      case '-':
        volume--;
        mp3_set_volume (volume);
        Serial.println(volume);
        break;
      case '+':
        if(volume < 28)
          volume++;
        mp3_set_volume (volume);
        Serial.println(volume);
        break;
      case 'p':
        radius = radius + 10;
        Serial.println(radius);
        break;
      case 'm':
        radius = radius - 10;
        Serial.println(radius);
        break;
      case 'x':
        rand0m = true;
        break;
      default:
        Serial.println("Zadal si sračku! Debile!");
        break;
    }

    //Direct play
    if (!playing && num < 50) {
      playing = true;
      mp3_play (num);
      delay (5500);
      mp3_stop ();
      playing = false;
    }
    
    //Change default record
    if(dflt != permanentRecord)
      permanentRecord = dflt;     
  }

  
}
